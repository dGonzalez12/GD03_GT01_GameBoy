using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

public class HighligthAnimation: MonoBehaviour{

	protected RectTransform _rectTransform;
	
	private Vector3 _originalScale = Vector3.one;
	private Vector3 _originalPosition;
	private Vector3 _originalRotation;

	protected Sequence _sequence;
	protected Sequence onceSequence;

	[SerializeField]
	protected float _animTime = 0.3f;

	[SerializeField]
	private Vector3 _targetScale;
	[SerializeField]
	private Vector3 _targetOffset;
	[SerializeField]
	private Vector3 _targetRotation;

	[SerializeField]
	protected Ease _animEase = Ease.InOutBack;


	private void Awake(){
		_rectTransform = GetComponent<RectTransform>();
		_originalPosition = _rectTransform.anchoredPosition3D;
		_originalScale = transform.localScale;
		_originalRotation = transform.localEulerAngles;
	}

	private void Start(){
		InitializeSequence();
	}

	virtual
	protected void InitializeSequence(){
		_sequence = DOTween.Sequence();
		_sequence.SetAutoKill(false);
		_sequence.Join(transform.DOScale(_targetScale,_animTime)).SetEase(_animEase);
		_sequence.Join(transform.DOLocalRotate(_targetRotation, _animTime)).SetEase(_animEase);
		_sequence.Pause();
	}

	virtual
	public void HighlighOn(){
		_sequence.PlayForward();
	}

	virtual
	public void HighlightOff(){
		_sequence.PlayBackwards();
	}

	virtual
	public void AnimateOnce(){
		onceSequence = DOTween.Sequence();
		onceSequence.Append(_rectTransform.DOLocalMove(_originalPosition + _targetOffset, _animTime * 2f)).SetEase(_animEase);
		onceSequence.Join(transform.DOScale(_targetScale,_animTime * 2f)).SetEase(_animEase);
		onceSequence.Join(transform.DOLocalRotate(_targetRotation, _animTime * 2f)).SetEase(_animEase);
		onceSequence.Append(_rectTransform.DOLocalMove(_originalPosition, _animTime * 2f)).SetEase(_animEase);
		onceSequence.Join(transform.DOScale(_originalScale,_animTime * 2f)).SetEase(_animEase);
		onceSequence.Join(transform.DOLocalRotate(_originalRotation, _animTime * 2f)).SetEase(_animEase);
		onceSequence.Play();
	}

	void Reset(){
		_originalScale = Vector3.one * 1.2f;
	}

}
