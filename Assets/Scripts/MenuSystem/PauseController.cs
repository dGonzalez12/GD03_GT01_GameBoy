using UnityEngine;
using UnityEngine.Events;
// using UnityEngine.InputSystem;

public class PauseController: MonoBehaviour{
    
	public static bool isPaused;
	[SerializeField]
	// private InputAction pauseAction;
	public static PauseController Instance;

	public UnityEvent onPaused;
	public UnityEvent onResumed;

	private void Awake()
	{
		Instance = this;
	}

	private void Start(){
		isPaused = false;
	}

	private void OnEnable(){
        // pauseAction.Enable();
        // pauseAction.performed += TogglePause;
        
    }

    private void OnDisable(){
        // pauseAction.performed -= TogglePause;
        // pauseAction.Disable();
    }

	public void TogglePause(){

	}

	/*
    public void TogglePause(InputAction.CallbackContext context){
        isPaused = !isPaused;
        if(isPaused){
            PauseGame();
        }else{
            UnPauseGame();
        }
    }
	*/

	
    public void PauseGame(){
        Time.timeScale = 0;
        onPaused?.Invoke();
        isPaused = true;
    }

    public void UnPauseGame(){
        Time.timeScale = 1f;
		onResumed?.Invoke();
        isPaused = false;
    }

}

