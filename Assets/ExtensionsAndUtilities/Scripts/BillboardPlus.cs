﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardPlus : MonoBehaviour
{
    [SerializeField] GameObject target;
    [SerializeField] bool isActive;
    [SerializeField] bool activeOnStart = true;
    //[Range(0f,20f)]
    //public float scale;
    void Start(){
        SetActive(activeOnStart);
    }

    void Update(){
        if(target && isActive){
            transform.LookAt(target.transform.position.WithHeightOf(transform.position));
        }
    }

    public void SetTarget(GameObject value){
        target = value;
    }

    public void SetActive(bool value){
        isActive = value;
    }
}
