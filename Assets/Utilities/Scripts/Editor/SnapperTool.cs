﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;

public class SnapperTool :EditorWindow{
    public float currentGridScale = 1f;
    public bool displayGrid;
    public int gridSize = 10;
    public int angularDivisions = 24;

    public float testNumber;

    public enum GridType{
        cartesian,
        polar
    };
    public GridType gridType = GridType.cartesian;
    
    SerializedObject so;
    SerializedProperty propertyGridScale;
    SerializedProperty propertyGridType;
    SerializedProperty propertyAngularDivissions;
    SerializedProperty propertyDisplayGrid;

    [MenuItem("Custom/Snapper Tool")]
    public static void OpenWindow(){
        GetWindow<SnapperTool>("Snap tool window custom title");
    }

    void OnEnable(){
        so = new SerializedObject(this);
        propertyGridScale = so.FindProperty("currentGridScale");
        propertyGridType = so.FindProperty("gridType");
        propertyAngularDivissions = so.FindProperty("angularDivisions");
        propertyDisplayGrid = so.FindProperty("displayGrid");
        //Add a callback to the selection. So if the selection changes, we repaint the UI
        Selection.selectionChanged += Repaint;
        SceneView.duringSceneGui += DuringSceneGUI;
        //How to save data to editor prefs
        //Load
        currentGridScale = EditorPrefs.GetFloat("SNAPPER_TOOL_gridScale",1f);
        gridType = (GridType)EditorPrefs.GetInt("SNAPPER_TOOL_gridType", 0);
        angularDivisions = EditorPrefs.GetInt("SNAPPER_TOOL_angularDivisions",24);
        displayGrid = EditorPrefs.GetInt("SNAPPER_TOOL_displayGrid") == 1;
    }

    void OnDisable(){
        //Unsuscribe from the selection change event
        Selection.selectionChanged -= Repaint;
        SceneView.duringSceneGui -= DuringSceneGUI;
        //Save the data
        EditorPrefs.SetFloat("SNAPPER_TOOL_gridScale", currentGridScale);
        EditorPrefs.SetInt("SNAPPER_TOOL_gridType",(int)gridType);
        EditorPrefs.SetInt("SNAPPER_TOOL_angularDivisions",angularDivisions);
        EditorPrefs.SetInt("SNAPPER_TOOL_displayGrid",displayGrid?1:0);
    }

    void DuringSceneGUI(SceneView sceneView){
        if(displayGrid){
            //Handles.DrawLine(Vector3.zero, Vector3.up);
            foreach (GameObject item in Selection.gameObjects){
                //Debug.Log(item.transform.position);
                //Handles.color = Color.red;
                //DrawGrid(item.transform.position);
                Handles.zTest = CompareFunction.LessEqual;
                Handles.color = new Color(0.2f,0.2f,1f,0.8f);
                if(gridType == GridType.cartesian){
                    DrawSphereGrid(item.transform.position);
                }else if (gridType == GridType.polar){
                    DrawPolarGrid();
                }
                Handles.color = new Color (0,0.5f,0.5f,0.5f);

                //Handles.DrawWireDisc(Round(item.transform.position, currentGridScale),Vector3.up, gridSize * 0.1f);
                //Handles.DrawSolidDisc(Round(item.transform.position, currentGridScale),Vector3.up, currentGridScale * 0.3f);
                Handles.zTest = CompareFunction.Always;
                Vector3 discPosition = Vector3.zero;
                switch (gridType){
                    case GridType.cartesian:
                        discPosition = item.transform.position.Round(currentGridScale);
                    break;
                    case GridType.polar:
                        discPosition = RoundPolar(item.transform.position, currentGridScale);
                    break;
                }

                Handles.DrawSolidDisc(
                    discPosition,
                    Vector3.up, 
                    HandleUtility.GetHandleSize(item.transform.position.Round(currentGridScale)) * 0.25f
                    );
            }
        }
    }

    void OnGUI(){
        //This statement disables everything inside IF the selection has no objects

        so.Update();
        //currentGridScale    = EditorGUILayout.FloatField("Grid Scale",currentGridScale);
        EditorGUILayout.PropertyField(propertyGridScale);
        EditorGUILayout.PropertyField(propertyGridType);
        EditorGUILayout.PropertyField(propertyDisplayGrid);
        if(gridType == GridType.polar){
            EditorGUILayout.PropertyField(propertyAngularDivissions);
            propertyAngularDivissions.intValue = Mathf.Max(4, propertyAngularDivissions.intValue);
        }
        if(so.ApplyModifiedProperties()){
            SceneView.RepaintAll();
        }

        using (new EditorGUI.DisabledScope(Selection.gameObjects.Length == 0)){    
            if(GUILayout.Button("Snap Selection")){
                SnapSelection();
            }
        }
    }

    void SnapSelection(){
        foreach (GameObject gameobject in Selection.gameObjects){
            Undo.RecordObject(gameobject.transform, "Snap Objects");
            //gameobject.transform.position = Round(gameobject.transform.position, currentGridScale);
            switch (gridType){
                case GridType.cartesian:
                    gameobject.transform.position = gameobject.transform.position.Round(currentGridScale);
                break;
                case GridType.polar:
                    //Snap distance to the origin;
                    gameobject.transform.position = RoundPolar(gameobject.transform.position, currentGridScale);
                break;
            }
        }
    }

    void DrawGrid(Vector3 startingPoint){
        startingPoint.y = 0;
        for(int i = - gridSize/2; i < (gridSize/2) + 1; i++){
            Handles.DrawLine(
                //Round(startingPoint + new Vector3(i * currentGridScale, 0, (-gridSize * 0.5f) * currentGridScale), currentGridScale),
                new Vector3(
                    startingPoint.x + (i * currentGridScale),
                    0,
                    startingPoint.z + ((-gridSize * 0.5f) * currentGridScale)
                ).Round(currentGridScale),
                new Vector3(
                    startingPoint.x + (i * currentGridScale),
                    0,
                    startingPoint.z + (gridSize * 0.5f) * currentGridScale
                ).Round(currentGridScale)
                //Round(startingPoint + new Vector3(i * currentGridScale, 0, (gridSize * 0.5f) * currentGridScale), currentGridScale)
            );
            Handles.DrawLine(
                new Vector3(
                    startingPoint.x + (gridSize * 0.5f) * currentGridScale,
                    0,
                    startingPoint.z + i * currentGridScale
                ).Round(currentGridScale),
                //Round(startingPoint + new Vector3((-gridSize * 0.5f) * currentGridScale, 0, i * currentGridScale), currentGridScale),
                new Vector3(
                    startingPoint.x + (-gridSize * 0.5f) * currentGridScale,
                    0,
                    startingPoint.z + i * currentGridScale
                ).Round(currentGridScale)
                //Round(startingPoint + new Vector3(( gridSize * 0.5f) * currentGridScale, 0, i * currentGridScale), currentGridScale)
            );
        }
    }

    void DrawSphereGrid(Vector3 startingPoint){
        startingPoint.y = 0;
        for (int i = -gridSize / 2; i < (gridSize / 2) + 1; i++){
            //X Axis 
            float a = i * currentGridScale;
            float c = gridSize * 0.5f * currentGridScale;
            float b = Mathf.Sqrt((c*c)-(a*a));
            //Vector3 lineAPoint = Round(startingPoint,currentGridScale) + new Vector3( b, 0, a);
            //Vector3 lineAPoint = startingPoint + new Vector3( b, 0, a);
            //Vector3 lineAPoint = new Vector3(startingPoint.x + b, 0, Round(startingPoint.z,currentGridScale) + a);
            Vector3 lineAPoint = new Vector3(startingPoint.x + b, 0, startingPoint.z.RoundF(currentGridScale) + a);

            //Vector3 lineBPoint = Round(startingPoint,currentGridScale) + new Vector3(-b, 0, a);
            //Vector3 lineBPoint = startingPoint + new Vector3(-b, 0, a);
            Vector3 lineBPoint = new Vector3(startingPoint.x -b, 0, startingPoint.z.RoundF(currentGridScale) + a);
            Handles.DrawLine(lineAPoint, lineBPoint);
            //Y Axis
            //Vector3 lineCPoint = Round(startingPoint,currentGridScale) + new Vector3( a, 0, b);
            //Vector3 lineCPoint = startingPoint + new Vector3( a, 0, b);
            Vector3 lineCPoint = new Vector3(startingPoint.x.RoundF(currentGridScale) + a, 0 ,startingPoint.z + b);

            // Vector3 lineDPoint = Round(startingPoint,currentGridScale) + new Vector3( a, 0,-b);
            //Vector3 lineDPoint = startingPoint + new Vector3( a, 0,-b);
            Vector3 lineDPoint = new Vector3(startingPoint.x.RoundF(currentGridScale) + a, 0 ,startingPoint.z - b);
            Handles.DrawLine(lineCPoint, lineDPoint);
        }
    }

    void DrawPolarGrid(){
        Vector3 startingPoint = Vector3.zero;
        for(int i = 0; i <= gridSize; i++){
            Handles.DrawWireDisc(startingPoint, Vector3.up, currentGridScale * i);
        }
        float c = currentGridScale * gridSize;
        Vector3 finalPoint = startingPoint;
        for(int i = 0; i < angularDivisions; i++){
            float currentAngle = i * (360f/angularDivisions);
            float co = Mathf.Sin(currentAngle * Mathf.Deg2Rad) * c;
            float ca = Mathf.Cos(currentAngle * Mathf.Deg2Rad) * c;
            finalPoint = startingPoint + new Vector3(co,0,ca);
            Handles.DrawLine(startingPoint,finalPoint);
        }
    }

    public Vector3 RoundPolar(Vector3 original, float scale){

        Vector2 vec = new Vector2(original.x, original.z);
        float dist = vec.magnitude;
        float distSnapped = dist.RoundF(currentGridScale);
        //Snap angle to the origin

        float angRad = Mathf.Atan2(vec.x, vec.y);
        float angTurns = angRad * Mathf.Rad2Deg;
        float angsnapped = angTurns.RoundF(360/angularDivisions);

        Vector3 newPosition = new Vector3(
            Mathf.Sin(angsnapped * Mathf.Deg2Rad) * distSnapped,
            original.y.RoundF(currentGridScale),
            Mathf.Cos(angsnapped * Mathf.Deg2Rad) * distSnapped
        );
        return newPosition;
    }

    

}
