﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TransformUtilities : EditorWindow{

    string newName = "";

    public GameObject toAdd;
    bool sameName;
    [MenuItem("Custom/Transfrom utilites")]
    public static void ShowWindow(){
        GetWindow<TransformUtilities>("Transform Utilites");
    }


    void OnGUI(){
        GUILayout.Label("Rename all transforms as:", EditorStyles.boldLabel);
        newName = EditorGUILayout.TextField("New name:", newName);
        sameName = EditorGUILayout.Toggle("Number them?", sameName);
        if (GUILayout.Button("Rename")){
            if(Selection.activeGameObject){
                RenameObjects(Selection.transforms, newName, !sameName);
            }else{
                Debug.Log("No object(s) selected");
            }
        }
        toAdd = (GameObject)EditorGUILayout.ObjectField("Object to Add", toAdd, typeof(GameObject), false);

        if(GUILayout.Button("Add To Transforms")){
            if (Selection.activeGameObject){
                if(toAdd){
                    AddToObject(Selection.transforms, toAdd);
                }
            }else{
                Debug.Log("No object(s) selected");
            }
        }

        if(GUILayout.Button("Unparent All")){
            UnparentGameObjects(Selection.gameObjects);
        }

        if(GUILayout.Button("Rename the children of this object")){
            if(Selection.activeGameObject){
                RenameChildren(Selection.activeGameObject.transform,newName);
            }
        }
    }

    public void AddToObject(Transform[] selectionObjects, GameObject objectToAdd){
        foreach (Transform item in selectionObjects){
            Instantiate(objectToAdd, item.transform.position, Quaternion.identity, item);
        }
    }

    public void RenameObjects(Transform[] objectsToRename, string name, bool sameName){
        // objectsToRename = OrganizeTransforms(objectsToRename);
        if(name.Equals("")){
            name = "GameObject";
        }
        int i = 0;
        foreach (Transform item in objectsToRename){
            string newN = name;
            if(!sameName){
                // newN += string.Format(" ({0})", i);
                newN += string.Format(" ({0})", item.GetSiblingIndex());
                i++;
            }
            item.name = newN;
        }
    }

    public void RenameChildren(Transform parent, string basename = "GameObject"){
        foreach (Transform item in parent){
            string newN = string.Format("{1} ({0})", item.GetSiblingIndex(), basename);
            item.name = newN;
        }
    }


    public Transform[] OrganizeTransforms(Transform[] objects){
        List<Transform> sortedList = new List<Transform>();
        if(!objects[0].parent){
            return objects;
        }
        for(int i = 0; i < objects.Length; i++){
            for(int j = 0; j < objects[0].parent.transform.childCount; j++){
                if(objects[i].GetInstanceID() == objects[0].parent.GetChild(j).GetInstanceID()){
                    sortedList.Add(objects[i]);
                }
            }
        }
        Transform[] sortedArray = new Transform[objects.Length];

        for(int i = 0; i < sortedList.Count; i++){
            sortedArray[i] = sortedList[i];
            Debug.Log("I" + i + " " + sortedArray[i].name);
        }

        return sortedArray;
    }


    public void UnparentGameObjects(GameObject[] selectionObjects){
        foreach (GameObject item in selectionObjects){
            item.transform.parent = null;
        }
    }

}
